<?php include 'header.php' ?>
<!-- Page content -->
<div id="content-body">
    <div class="container">
        <div class="row">
            <!-- Primary content -->
            <div id="primary-content" class="col-xs-12 col-lg-10">
                <h2>Headings</h2>
                <h1>Heading 1</h1>
                <h2>Heading 2</h2>
                <h3>Heading 3</h3>
                <h4>Heading 4</h4>
                <h5>Heading 5</h5>
                <h6>Heading 6</h6>
                <h2>Paragraph / Body text</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse posuere leo placerat sem rutrum, sed mattis est rhoncus. Quisque dignissim fermentum erat non facilisis. Maecenas id metus sit amet augue facilisis gravida eget egestas ligula. Sed ut pharetra sapien. Phasellus eleifend est sapien. Vestibulum congue metus nec ultrices laoreet. Proin iaculis congue fermentum. Praesent mi nisi, porta accumsan gravida sit amet, pellentesque non lectus. Aenean urna nunc, facilisis vitae porttitor et, dapibus sit amet augue. Donec quis velit et sapien euismod sagittis nec in justo. Mauris eget nisl eget eros maximus commodo nec ut velit. Etiam ipsum mauris, feugiat nec justo ullamcorper, dapibus convallis augue. Donec dictum venenatis ante in bibendum. Donec rutrum dictum sem et maximus. Maecenas sit amet elementum turpis, quis commodo lorem.</p>
                <p>Mauris et turpis at mi finibus tristique. Integer lobortis mauris eget libero tincidunt egestas. Aliquam erat volutpat. Donec maximus blandit justo et volutpat. Curabitur et velit in mauris dapibus consequat. Aenean in auctor nibh. Aliquam imperdiet laoreet ullamcorper. Integer quis lectus blandit, bibendum felis ut, ullamcorper risus. In blandit, sapien quis aliquet finibus, ligula purus varius ipsum, ut feugiat metus ante sit amet metus. Curabitur at porttitor eros. In interdum metus ut ante ullamcorper, sit amet iaculis ipsum maximus. Donec et risus ipsum. Cras ligula massa, gravida et hendrerit quis, tempus in libero. Integer nibh felis, tristique non mollis ac, aliquet ut neque. Proin placerat ultricies placerat. Sed non mauris sed mi rutrum luctus in et dui.</p>
                <h2>Lists</h2>
                <ul>
                    <li>List item 1</li>
                    <li>List item 2</li>
                    <li>List item 3</li>
                    <li>List item 4</li>
                    <li>List item 5</li>
                </ul>
            </div>
            <?php include 'sidebar.php' ?>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>