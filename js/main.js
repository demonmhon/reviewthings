var App = function () {
    return this.init();
};

App.prototype.init = function () {
    var self = this;

    self.headerNavBarUI();
    self.searchUI();
}

App.prototype.headerNavBarUI = function () {
    var self = this;

    var header       = $('#header');
    var headerNavBar = $('#header').find('.navbar-collapse');

    $('body').on('mouseup touchmove', function (e) {
        var target   = headerNavBar.is(e.target);
        var contain  = header.has(e.target).length;
        if ( !target && !contain ) {
            headerNavBar.collapse('hide');
        }
    });
};
App.prototype.searchUI = function () {
    var self = this;

    var header    = $('#header');
    var searchBox = $('.search-box');
    var button    = $('#header').find('.search').find('a');
    var search    = $('#search');

    button.on('click', function () {
        header.addClass('search-active');
        searchBox.toggleClass('active');
        search.focus();
        return false;
    });

    $('body').on('mouseup touchmove', function (e) {
        var isButton = button.is(e.target);
        var target   = searchBox.is(e.target);
        var contain  = searchBox.has(e.target).length;
        if ( !isButton && !target && !contain ) {
            header.removeClass('search-active');
            searchBox.removeClass('active');
            search.blur();
        }
    });
}

var app;
$(document).ready( function () { app = new App(); } );