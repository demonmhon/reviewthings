    <!-- Footer -->
    <div id="footer">
        <div class="global-footer-nav">
            <div class="container">
                <div class="row">

                </div>
            </div>
        </div>
        <div class="footer-text">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <a class="navbar-brand" href="#">Review<span>Things</span></a>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <span class="copy-right">Copyright &copy; 2011-2014 ReviewThings Corp. All rights reserved </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>