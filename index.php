<?php include 'header.php' ?>
<!-- Page content -->
<div id="content-body">
    <div class="container">
        <div class="row">
            <!-- Primary content -->
            <div id="primary-content" class="col-xs-12 col-lg-10">
                <?php include 'banner-index.php' ?>
                <!-- Focus -->
                <div class="row">
                    <div class="col-xs-12 col-sm-6 focus">
                        <div class="row">
                            <div class="content-item col-xs-12">
                                <div class="content-item-box">
                                    <div class="title">
                                        <h1><a href="#">First time at Why-Wine, Pattaya, Thailand</a></h1>
                                        <span class="date">Dec 28, 2019</span>
                                    </div>
                                    <div class="info">
                                        <div class="feature-img">
                                            <img src="img/sample-002-slide.png" alt="" class="img-responsive">
                                        </div>
                                        <div class="response">
                                            <a href="#" class="rating">
                                                <span>4.5</span><span class="hidden-xs unit"> star</span>
                                            </a>
                                            <a href="#"  class="comment-count">
                                                <span>888</span><span class="hidden-xs unit"> comments</span>
                                            </a>
                                            <a href="#" class="tag visible-xs">
                                                <span>4</span><span class="hidden-xs unit"> tags</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae porta tortor, eget scelerisque arcu. Aenean porta volutpat justo. Morbi porta ex a leo sollicitudin, luctus blandit .. <a href="#">More</a></p>
                                    </div>
                                    <div class="meta hidden-xs">
                                        <div class="tag">
                                            <a href="#">wine</a>, <a href="#">drink</a>,<a href="#"> pattaya</a>, <a href="#">thailand</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-item col-xs-12">
                                <div class="content-item-box">
                                    <div class="title">
                                        <h1><a href="#">It's STEAK time! Pork Chop super combo at Best Beef Bar</a></h1>
                                        <span class="date">Dec 28, 2019</span>
                                    </div>
                                    <div class="info">
                                        <div class="feature-img">
                                            <img src="img/sample-003-slide.png" alt="" class="img-responsive">
                                        </div>
                                        <div class="response">
                                            <a href="#" class="rating">
                                                <span>5.0</span><span class="hidden-xs unit"> star</span>
                                            </a>
                                            <a href="#"  class="comment-count">
                                                <span>456</span><span class="hidden-xs unit"> comments</span>
                                            </a>
                                            <a href="#" class="tag visible-xs">
                                                <span>7</span><span class="hidden-xs unit"> tags</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae porta tortor, eget scelerisque arcu. Aenean porta volutpat justo. Morbi porta ex a leo sollicitudin, luctus blandit .. <a href="#">More</a></p>
                                    </div>
                                    <div class="meta hidden-xs">
                                        <div class="tag">
                                            <a href="#">pork</a>, <a href="#">pork chop</a>,<a href="#"> steak</a>, <a href="#">deal</a>, <a href="#">best beef bar</a>, <a href="#">a la carte</a>, <a href="#">resturant</a>
                                        </div>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Focus -->                                   
                    <!-- Additional -->
                    <div class="col-xs-12 col-sm-6 additional">
                        <div class="row">
                            <div class="content-item col-xs-12">
                                <h1 class="title bar"><a href="#">5 Star Review</a></h1>
                                <div class="content-item-box">
                                    <div class="row">
                                        <div class="col-xs-8 text-content">
                                            <div class="title">
                                                <h2><a href="#">Piszza XXX, secret recipe. The BRAND NEW TASTE!!</a></h2>
                                            </div>
                                            <div class="body">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae porta tortor, eget scelerisque arcu. Aenean porta volutpat justo .. <a href="#">More</a></p>
                                            </div>
                                            <div class="info">
                                                <span class="date">Dec 28, 2019</span>
                                                <span class="comment-count">
                                                    <a href="#">888 comments</a>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 feature-img">
                                            <img src="img/sample-003-slide.png" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                                <div class="content-item-box">
                                    <div class="row">
                                        <div class="col-xs-8 text-content">
                                            <div class="title">
                                                <h2><a href="#">The Green Place, restuarant review: 2hours buffet</a></h2>
                                            </div>
                                            <div class="body">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae porta tortor, eget scelerisque arcu. Aenean porta volutpat justo .. <a href="#">More</a></p>
                                            </div>
                                            <div class="info">
                                                <span class="date">Dec 28, 2019</span>
                                                <span class="comment-count">
                                                    <a href="#">888 comments</a>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 feature-img">
                                            <img src="img/sample-003-slide.png" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                                <div class="content-item-box">
                                    <div class="row">
                                        <div class="col-xs-8 text-content">
                                            <div class="title">
                                                <h2><a href="#">Special Interview: George McGuay, spaghetti best chef</a></h2>
                                            </div>
                                            <div class="body">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae porta tortor, eget scelerisque arcu. Aenean porta volutpat justo .. <a href="#">More</a></p>
                                            </div>
                                            <div class="info">
                                                <span class="date">Dec 28, 2019</span>
                                                <span class="comment-count">
                                            <a href="#">888 comments</a>
                                        </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 feature-img">
                                            <img src="img/sample-003-slide.png" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-item col-xs-12">
                                <h1 class="title bar"><a href="#">Top Comment</a></h1>
                                <div class="content-item-box">
                                    <div class="row">
                                        <div class="col-xs-12 text-content">
                                            <div class="title">
                                                <h2><a href="#">Special Interview: George McGuay, spaghetti best chef</a></h2>
                                            </div>
                                            <div class="info">
                                                <span class="date">Dec 28, 2019</span>
                                                <span class="comment-count">
                                                    <a href="#">888 comments</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-item-box">
                                    <div class="row">
                                        <div class="col-xs-12 text-content">
                                            <div class="title">
                                                <h2><a href="#">Special Interview: George McGuay, spaghetti best chef</a></h2>
                                            </div>
                                            <div class="info">
                                                <span class="date">Dec 28, 2019</span>
                                                <span class="comment-count">
                                                    <a href="#">888 comments</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-item-box">
                                    <div class="row">
                                        <div class="col-xs-12 text-content">
                                           <div class="title">
                                                <h2><a href="#">Special Interview: George McGuay, spaghetti best chef</a></h2>
                                            </div>
                                            <div class="info">
                                                <span class="date">Dec 28, 2019</span>
                                                <span class="comment-count">
                                                    <a href="#">888 comments</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       </div>
                    </div>
                    <!-- End Additional -->
                </div>
            </div>
            <?php include 'sidebar.php' ?>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>