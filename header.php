<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ReviewThings</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/main.all.min.js"></script>
    <link rel="stylesheet" href="css/main.all.min.css">
</head>
<body class="<?php if ( $_SERVER['REQUEST_URI'] == '/reviewthings/' ) { ?>home-page<?php } ?>">
    <!-- Header -->
    <div id="header">
        <div class="navbar navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                    <a class="navbar-brand" href="#">Review<span>Things</span></a>
                </div>
                
                <div class="search-box">
                    <form action="">
                        <div class="form-group">
                            <input type="search" id="search" class="form-control" name="keyword" autocomplete="off" placeholder="Search in ReviewThings" value="" >
                            <button type="submit" class="btn">Toggle Search</button>
                        </div>
                    </form>
                </div>
                <div class="xs-search visible-xs">
                    <ul class="nav">
                        <li class="search">
                            <a href="#">
                                <span>Search</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#">
                                <span>Review</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Video</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Directory</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Special Scoop</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Sponsor</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>About</span>
                            </a>
                        </li>
                        <li class="search">
                            <a href="#">
                                <span>Toggle Search</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>