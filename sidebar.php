<!-- Secondary content -->
<div id="secondary-content" class="col-xs-12 col-lg-2">
    <div class="row">
        <div class="content-item col-xs-12 col-sm-6 col-lg-12">
            <div class="side-box-content additional">
                <h2 class="title bar"><a href="#">Archive</a></h2>
                <div class="side-box-body">
                    <ul>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-item col-xs-12 col-sm-6 col-lg-12">
            <div class="side-box-content additional">
                <h2 class="title bar"><a href="#">Recent comment</a></h2>
                <div class="side-box-body">
                    <ul>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                        <li>
                            <div class="title"><h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h3></div>
                            <div class="info">
                                <span class="date">Dec 28, 2019</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>