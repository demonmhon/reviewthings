<!-- Rotate content -->
<div class="row">
    <div class="col-md-12">
        <div id="index-rotate-banner" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#index-rotate-banner" data-slide-to="0" class="active"></li>
                <li data-target="#index-rotate-banner" data-slide-to="1"></li>
                <li data-target="#index-rotate-banner" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <a href="#"><img src="img/sample-001-slide.png" alt="" class="img-responsive"></a>
                    <div class="carousel-caption">
                        <p>Best lunch set at JC resturant, Bangkok</p>
                    </div>
                </div>
                <div class="item">
                    <a href="#"><img src="img/sample-002-slide.png" alt="" class="img-responsive"></a>
                    <div class="carousel-caption">
                        <p>First time at Why-Wine, Pattaya, Thailand</p>
                    </div>
                </div>
                <div class="item">
                    <a href="#"><img src="img/sample-003-slide.png" alt="" class="img-responsive"></a>
                    <div class="carousel-caption">
                        <p>It's STEAK time! Pork Chop super combo at Best Beef Bar</p>
                    </div>
                </div>
            </div>

              <!-- Controls -->
            <a class="left carousel-control" href="#index-rotate-banner" role="button" data-slide="prev">
                <span></span>
            </a>
            <a class="right carousel-control" href="#index-rotate-banner" role="button" data-slide="next">
                <span></span>
            </a>
        </div>
   </div>
</div>